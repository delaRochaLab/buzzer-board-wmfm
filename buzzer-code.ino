/* Buzzer controller for ATTINY85. We have two Bpod outputs (valve and LED)
connected to two attiny inputs (pins 3 and 4). Upon level change, it triggers an ISR that
enables Timer 0 in CTC mode and outputs a 1 or 10 kHz PWM on pin 0. */

#include <avr/io.h>
#include <avr/interrupt.h>

int main(void) {
    // Timer 0 configuration (starts disabled, see ISR below):
    //TCCR0A |= (1 << COM0A0);              // Toggle OC0A output on compare match.
    //TCCR0A |= (1 << WGM01);               // Clear counter on compare match.
    // Pin configurations:
    DDRB &= ~(1 << DDB4 | 1 << DDB3);     // Pins 3 and 4 as inputs.
    DDRB |= (1 << DDB0);                  // Pin 0 as output. 
    // Interrupt configuration:
    GIMSK |= (1 << PCIE);                  // Pin change interrupt enable.
    PCMSK |= (1 << PCINT4 | 1 << PCINT3);  // Pin change interrupt enabled for PCINT4 and PCINT3.
    sei();                                 // Enable interrupts. 
    while(1){};
}

ISR(PCINT0_vect) {
     // Interrupt vector routine, called on interrupts from PCINT3 or PCINT4.
     if ((PINB >> 3) & 1){
        TCCR0A = (1 << COM0A0) | (1 << WGM01);
        TCCR0B = 0x01; // Activate the timer without prescaler --> 8MHz
        OCR0A = 200; // Compare match at 0.05 ms = 20 kHz.
     } else if ((PINB >> 4) & 1){
        TCCR0A = (1 << COM0A0) | (1 << WGM01);
        TCCR0B = 0x02; // Activate the timer with prescaler /8 --> 1MHz
        OCR0A = 125; // Compare match at 0.25 ms = 4 kHz.
     } else {
        TCCR0A = 0x00;
        TCCR0B = 0x00;
        PORTB &= ~(1 << PORTB0);
     } 
}
